<?php 
include "db.inc.php"; //database connection

$sql = "SELECT suppId, suppName, suppStreet FROM Supplier";

if (!$result = mysqli_query($con, $sql))
{
	die('Error in querying the database' . mysqli_error($con));
}

echo "<br><select name = 'listbox' id = 'listbox' onclick = 'populate()'>";

while ($row = mysqli_fetch_array($result))
{
	$id = $row['suppId'];
	$name = $row['suppName'];
	$street = $row['suppStreet'];
	$town = $row['suppTown'];
	$county = $row['suppCounty'];
	$phoneno = $row['suppPhoneNumber'];
	$faxno = $row['suppFaxNumber'];
	$email = $row['suppEmail'];
	$webadd = $row['suppWebAddress'];
	$allText = "$id,$name,$street,$town,$county,$phoneno,$faxno,$email,$webadd";
	echo "<option value = '$allText'>$name $street </option>";
}

echo "</select>";
mysqli_close($con);

?>