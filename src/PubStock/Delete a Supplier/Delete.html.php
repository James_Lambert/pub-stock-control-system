<head>
<style>
form
		{
			margin: 1em;
			max-width: 500px;
		}
label
		{
			display: inline-block;
			width: 9em;
			margin-right: 1em;
			margin-top: 1em;
			text-align: right;
		}

</style>
</head>
<body>

//<?php 
//			include 'menu.php';
//?>

<h1> Delete a Person</h1>
<h4> Please select a person and then click the delete button</h4>

<?php include 'listbox.php'; ?>

<script>
function populate()
{
	var sel = document.getElementById("listbox");
	var result;
	result = sel.options[sel.selectedIndex].value;
	var personDetails = result.split(',');
	document.getElementById("display").innerHTML = "The details of the selected person are: " + result;
	document.getElementById("delid").value = personDetails[0];
	document.getElementById("delfirstname").value = personDetails[1];
	document.getElementById("dellastname").value = personDetails[2];
	document.getElementById("delDOB").value = personDetails[3];
}

function confirmCheck()
{
	var response;
	response = confirm('Are you sure you want to delete this person?');
	if (response)
	{
		document.getElementById("delid").disabled = false;
		document.getElementById("delfirstname").disabled = false;
		document.getElementById("dellastname").disabled = false;
		document.getElementById("delDOB").disabled = false;
		return true;
	}
	else
	{
		populate();
		return false;
	}
}
</script>
<p id = "display"> </p>

<form name="deleteForm" action="delete.php" onsubmit="return confirmCheck()" method="post">

<label for "delid">Person Id </label>
<input type = "text" name = "delid" id = "delid" disabled>
<label for "delfirstname">First Name </label>
<input type = "text" name = "delfirstname" id = "delfirstname" disabled>
<label for "dellastname">Surname </label>
<input type = "text" name = "dellastname" id = "dellastname" disabled>
<label for "delDOB">Date of Birth </label>
<input type = "date" name = "delDOB" id "delDOB" title = "format is dd-mm-yyyy" disabled>
<br><br>
<input type = "submit" value = "Delete the record" >
</form>
</body>
</html>