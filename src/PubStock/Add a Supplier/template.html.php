<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="template.css">
<script src="template.js"></script>
<link rel="shortcut icon" href="resources/icon.png" />
<title> Add a New Supplier </title>
</head>

<body>

<img src="resources/PubStock.png" alt="PubStock" class="PubImage">
<br>
	<div class="navbar">
	  <a href="menu.html">Main Menu</a>
	  <a href="#">Counter Sales</a>
	  <div class="dropdown">
			 <button class="dropbtn">Stock Control</button>
			<div class="dropdown-content">
 			 <a href="#">Manual Ordering</a>
 			 <a href="#">Receive Goods</a>
 			 <a href="#">Reconcile Stock Figures</a>
 		 </div>
	  </div>
		<div class="dropdown">
		<button class="dropbtn">File Maintenance</button>
			<div class="dropdown-content">
					<a href="#">Add a New Staff member</a>
					<a href="#">Delete a Staff member</a>
					<a href="#">Amend / View a staff member</a>
					<a href="#">Add a New Supplier</a>
					<a href="#">Delete a Supplier</a>
					<a href="#">Amend / View a Supplier</a>
					<a href="#">Add a New Stock Item</a>
					<a href="#">Delete a Stock Item</a>
					<a href="#">Amend / View a Stock Item</a>
					<a href="#">Add a New Event</a>
					<a href="#">Delete an Event</a>
					<a href="#">Amend / View an Event</a>
				</div>
		</div>
		<div class="dropdown">
		<button class="dropbtn">Reports</button>
						<div class="dropdown-content">
					<a href="#">Stock Sheet</a>
					<a href="#">Sales Report</a>
					<a href="#">Barperson / Sales Report</a>
					<a href="#">End of Day Report</a>
					<a href="#">Events Report</a>
				</div>
		</div>
		<a href="#home">Change Password</a>
	 <a href="#news">Quit</a>
	</div>

<script>
function populate()
{
	var sel = document.getElementById("listbox");
	var result;
	result = sel.options[sel.selectedIndex].value;
	var personDetails = result.split(',');
	document.getElementById("display").innerHTML = "The details of the selected person are: " + result;
	document.getElementById("delid").value = personDetails[0];
	document.getElementById("delfirstname").value = personDetails[1];
	document.getElementById("dellastname").value = personDetails[2];
	document.getElementById("delDOB").value = personDetails[3];
}

function confirmCheck()
{
	var response;
	response = confirm('Are you sure you want to delete this person?');
	if (response)
	{
		document.getElementById("delid").disabled = false;
		document.getElementById("delfirstname").disabled = false;
		document.getElementById("dellastname").disabled = false;
		document.getElementById("delDOB").disabled = false;
		return true;
	}
	else
	{
		populate();
		return false;
	}
}
</script>	
	

<div class="content">

<?php include 'listbox.php'; ?>

<form name="deleteForm" action="delete.php" onsubmit="return confirmCheck()" method="post">
<p>
<label for "delid">Supplier Id </label>
<input type = "text" name = "delid" id = "delid" disabled>
</p><p>
<label for "delname">Supplier Name </label>
<input type = "text" name = "delname" id = "delname" disabled>
</p><p>
<label for "delstreet">Street </label>
<input type = "text" name = "delstreet" id = "delstreet" disabled>
</p><p>
<label for "deltown">Town </label>
<input type = "text" name = "deltown" id = "deltown" disabled>
</p><p>
<label for "delcounty">County </label>
<input type = "text" name = "delcounty" id = "delcounty" disabled>
</p><p>
<label for "delphoneno">Phone number </label>
<input type = "tel" name = "delphoneno" id = "delphoneno" disabled>
</p><p>
<label for "delfaxno">Fax number </label>
<input type = "tel" name = "delfaxno" id = "delfaxno" disabled>
</p><p>
<label for "delemail">Email </label>
<input type = "email" name = "delemail" id = "delemail" disabled>
</p><p>
<label for "delwebadd">Web address </label>
<input type = "text" name = "delwebadd" id = "delwebadd" disabled>
</p>
<br>

		<input type="submit" value = "Submit"/>
		<input type="reset" value = "Clear"/>

<p>
</form>


</div>

</body>
</html>
