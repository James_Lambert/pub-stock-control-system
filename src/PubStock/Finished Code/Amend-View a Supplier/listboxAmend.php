<?php 
include "db.inc.php"; //database connection
//sql staement to select all the colums from supplier table 
$sql = "SELECT suppId, suppName, suppStreet, suppTown, suppCounty, suppPhoneNumber, suppFaxNumber, suppEmail, suppWebAddress FROM Supplier";
//test query and connection if it doesnt wor error 
if (!$result = mysqli_query($con, $sql))
{
	die('Error in querying the database' . mysqli_error($con));
}
//start of listbox 
echo "<br><select name = 'listbox' id = 'listbox' onclick = 'populate()'>";
//get data from table as rows and show the in listbox 
while ($row = mysqli_fetch_array($result))
{
	$id = $row['suppId'];
	$name = $row['suppName'];
	$street = $row['suppStreet'];
	$town = $row['suppTown'];
	$county = $row['suppCounty'];
	$phoneno = $row['suppPhoneNumber'];
	$faxno = $row['suppFaxNumber'];
	$email = $row['suppEmail'];
	$webadd = $row['suppWebAddress'];
	$allText = "$id,$name,$street,$town,$county,$phoneno,$faxno,$email,$webadd";
	echo "<option value='' disabled selected hidden>Choose a Supplier...</option>";
	echo "<option value = '$allText'>$name </option>";
}
//end of listbox 
echo "</select>";
//close database connection
mysqli_close($con);

?>