<?php include 'template.php'; ?><!--include the template document which has the menu bar -->
 <html>
 <head>
   <title> Amend Supplier</title>
<link rel="stylesheet" type="text/css" href="template.css">
</head>
<body>

<script>
function populate() //populates the fields on screen with the data from the Supplier table 
	{
		var sel = document.getElementById("listbox");
		var result;
		result = sel.options[sel.selectedIndex].value;
		var supplierDetails = result.split(',');

		document.getElementById("amendId").value = supplierDetails[0];
		document.getElementById("amendname").value = supplierDetails[1];
		document.getElementById("amendstreet").value = supplierDetails[2];
		document.getElementById("amendtown").value = supplierDetails[3];
		document.getElementById("amendcounty").value = supplierDetails[4];
		document.getElementById("amendphoneno").value = supplierDetails[5];
		document.getElementById("amendfaxno").value = supplierDetails[6];
		document.getElementById("amendemail").value = supplierDetails[7];
		document.getElementById("amendwebadd").value = supplierDetails[8];

	}

//If you hit the amend buuton the input box will toggle open or close 
function toggleLock()
	{
		if (document.getElementById("amendViewbutton").value == "Amend Details")
			{
				document.getElementById("amendId").disabled = false;
				document.getElementById("amendname").disabled = false;
				document.getElementById("amendstreet").disabled = false;
				document.getElementById("amendtown").disabled = false;
				document.getElementById("amendcounty").disabled = false;
				document.getElementById("amendphoneno").disabled = false;
				document.getElementById("amendfaxno").disabled = false;
				document.getElementById("amendemail").disabled = false;
				document.getElementById("amendwebadd").disabled = false;	

				document.getElementById("amendViewbutton").value="View Details";
			}
		else
			{
			  	document.getElementById("amendId").disabled = true;
			  	document.getElementById("amendname").disabled = true;
			  	document.getElementById("amendstreet").disabled = true;
			  	document.getElementById("amendtown").disabled = true;
			  	document.getElementById("amendcounty").disabled = true;
			  	document.getElementById("amendphoneno").disabled = true;
			  	document.getElementById("amendfaxno").disabled = true;
			  	document.getElementById("amendemail").disabled = true;
				document.getElementById("amendwebadd").disabled = true;		

			  	document.getElementById("amendViewbutton").value="Amend Details";
			}
	}

//confirm message on screen when you hit the submit button
function confirmCheck()
	{
		var response;
		response = confirm('Are you sure you want to save these changes?');
		
		if (response)
			{
    			document.getElementById("amendId").disabled = false;
				document.getElementById("amendname").disabled = false;
				document.getElementById("amendstreet").disabled = false;
				document.getElementById("amendtown").disabled = false;
				document.getElementById("amendcounty").disabled = false;
				document.getElementById("amendphoneno").disabled = false;
				document.getElementById("amendfaxno").disabled = false;
				document.getElementById("amendemail").disabled = false;
				document.getElementById("amendwebadd").disabled = false;
  				return true;
 			}
		else
 			{
				populate();
				toggleLock();
				return false;
 			}
	}
</script>
	<!--start of css style sheet in the template-->
	<div class="content">
		<!--heading 1 -->
		<h1> Amend/View a Supplier</h1>
		<!--heading 4 -->
		<h4>Please select item and then click the amend button if you wish to update the record</h4>
		<!--include or show on screen the listbox that contains the supplier information-->
		<?php include 'listboxAmend.php'; ?>
		<p id = "display"> </p>
		<!--amend button-->
		<input type ="button" value="Amend Details" id="amendViewbutton" onclick="toggleLock()">
		<!--form start-->
		<form name="amendForm" action="AmendSupplier.php" onsubmit="return confirmCheck()" method="post">
		<br><br>
		<!--field that contains the supplier id -->
		<label for "amendId">Supplier Id </label>
		<input type = "text" name = "amendId" id = "amendId" disabled>
		<br><br>
		<!--field that contains the supplier name -->
		<label for "amendname">Name </label>
		<input type = "text" name = "amendname" id = "amendname" disabled>
		<br><br>
		<!--field that contains the supplier street -->
		<label for "amendstreet">Street </label>
		<input type = "text" name = "amendstreet" id = "amendstreet" disabled>
		<br><br>
		<!--field that contains the supplier town -->
		<label for "amendtown">Town </label>
		<input type = "text" name = "amendtown" id = "amendtown" disabled>
		<br><br>
		<!--field that contains the supplier county-->
		<label for "amendcounty">County </label>
		<input type = "text" name = "amendcounty" id = "amendcounty" disabled>
		<br><br>
		<!--field that contains the supplier phone number -->
		<label for "amendphoneno">Phone Number </label>
		<input type = "tel" name = "amendphoneno" id = "amendphoneno" disabled>
		<br><br>
		<!--field that contains the supplier fax number -->
		<label for "amendfaxno">Fax Number </label>
		<input type = "tel" name = "amendfaxno" id = "amendfaxno" disabled>
		<br><br>
		<!--field that contains the supplier email-->
		<label for "amendemail">Email </label>
		<input type = "email" name = "amendemail" id = "amendemail" disabled>
		<br><br>
		<!--field that contains the supplier web address -->
		<label for "amendwebadd">Web Address </label>
		<input type = "text" name = "amendwebadd" id = "amendwebadd" disabled>
		<br><br>	
			<!--submit button that will go to php page -->
			<input type="submit" value = "Submit"/>
			<!--reset data on screen-->
			<input type="reset" value = "Clear"/>


		</form><!--form end -->

	</div><!--css end -->
</body>
</html>
