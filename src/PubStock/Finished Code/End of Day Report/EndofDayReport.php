<?php
//Start of a new session called endOfDaySession
session_id('endOfDaySession');
session_start();
?>
<html>
<head>
<!--link to css style sheet-->
<link rel="stylesheet" type="text/css" href="template.css">
<title> End of Day Report </title>
</head>
<body>
<?php 	include 'template.php';		//include and show a template document that contains menu bar
		include 'db.inc.php';		//database connection
		date_default_timezone_set('UTC');	//timezone set to Irish time
?>
<!--starts a new div class that styles the page -->
<div class= "content">
	<!--form start-->
 	<form action="EndofDayReport.php" method="post" name="endOfDayForm">
	<!--hidden input box that contains the user choice of button -->
	<input type="hidden" name="choice">
	<!--heading-->
	<h2>End of Day Report</h2>
	<h4>(Click a button to see the Stock data in the desired order)</h4>
	<!--button that contains category, alows user to see category in ascending order-->
	<input type="button" id="categoryBtn" value='category' onclick="categoryOrder()"
 	title="Click here to see Stock in ascending order of Category">
	<!--button that contains value, alows user to see category in descending order-->
	<input type="button" id="valueBtn" value='value' onclick="valueOrder()"
  	title="Click here to see Stock in the order of Supplier value in descending order">
	<br></br>
	Enter a From-date:
	<!-- date picker-->
  	<input type="date" name="dateForm">
  	<input type="submit" value="Send" id="send">
	<br></br>
	<!--print button that prints the form -->
	<div class = "print">
		<input type="button" id="print" onclick="window.print()" value="print" />
	</div>
</div>
<script>
function categoryOrder()	//javascript function to show in category order
{
	document.endOfDayForm.choice.value = "category";
	document.endOfDayForm.submit();
}
function valueOrder()	//javascript function to show in value order 
{
	document.endOfDayForm.choice.value = "value";
	document.endOfDayForm.submit();
}
	</script>

<?php
//sets the date to today 
$date = date('Y-m-d');
//gets tomorrows date 
$tomorrow = date('Y-m-d',strtotime($_POST['dateForm'] . "+1 days"));

		
//Time
$d = strtotime("04:00pm");
$time = date("h:ia", $d);
$d2 = strtotime("11:30pm");
$time2 = date("h:ia", $d2);

//the choice is set to category by default
$choice ="category";
//if the choice is category 
if (ISSET($_POST['choice']))
{
	$choice=$_POST['choice']; //set the post or hidden input box to category 
}
	//if the choice is value , nested if statement 
	if ($choice =="value")
	{
		?>
		<script>
		//category button is disabled
		document.getElementById("categoryBtn").disabled = false;
		//value button is not 
		document.getElementById("valueBtn").disabled = true;
		</script>

		<?php
		
		//value choice
		//if the date IS set 
		if(ISSET($_SESSION["dateAndTime"])) 
		{
			//sql statement that uses the session variable stored when the date is change via the date picker 
			//gets all from sales , join sales and stock where the date is equal to the session variable 
			//orders retail price by descending order 
			$sql = "SELECT * FROM Sales INNER JOIN Stock ON Sales.stockId=Stock.stockNumber 
			WHERE dateAndTime = '$_SESSION[dateAndTime]' ORDER BY retailPricePerUnit DESC";
			//table header that prints the date 
			echo "<table>".
						"<tr>
							<th>From: ".$_SESSION["dateAndTime"]. " ".$time."</th>		
							<th>To: " .$_SESSION["dateAndTime"]." ".$time2."</th>
						<tr>".
				 "</table>";
			//execute function to print database data 
			productReport($con,$sql);
		}
		//if the date is not set 
		else
		{
			//sql statement that uses the session variable stored when the date is change via the date picker 
			//gets all from sales , join sales and stock where the date is equal to the session variable 
			//orders retail price by descending order 
			$sql = "SELECT * FROM Sales INNER JOIN Stock ON Sales.stockId=Stock.stockNumber 
			WHERE dateAndTime = '$_POST[dateForm]' ORDER BY retailPricePerUnit DESC";
			//table header that prints the date 
			echo "<table>".
						"<tr>
							<th>From: ".$_SESSION["dateAndTime"]. " ".$time."</th>		
							<th>To: " .$_SESSION["dateAndTime"]." ".$time2."</th>
						<tr>".
				"</table>";
			//execute function to print database data 
			productReport($con,$sql);
		}
			
	}
	//else eg the choice is category , nested else 
	else
	{
		?>
		<script>
		//category button is disabled 
		document.getElementById("categoryBtn").disabled = true;
		//value button is not 
		document.getElementById("valueBtn").disabled = false;
		</script>
	<?php
		
		
		//choice of category
		//if the date is NOT set when category is chosen 
		if(!ISSET($_POST['dateForm'])) 
		{
			//sets the date to today 
			$date = date("Y-m-d");
			//the post data from the date picker is set to today 
			$_POST['dateForm'] = $date;
			//sql statement that uses the post variable that stores todays date 
			//gets all from sales , join sales and stock where the date is equal to the session variable 
			//orders category by ascending order 
			$sql = "SELECT * FROM Sales INNER JOIN Stock ON Sales.stockId=Stock.stockNumber 
			WHERE dateAndTime = '$_POST[dateForm]' ORDER BY stockCategory ASC";
			//table header that prints the date 
			echo "<table>".
						"<tr>
							<th>From: ".$_POST['dateForm']. " ".$time."</th>		
							<th>To: " .$_POST['dateForm']." ".$time2."</th>
						<tr>".
				"</table>";
			//execute function to print database data
			productReport($con,$sql);
		}
		//else the date IS set 
		else
		{
			//sql statement that uses the post variable that stores todays date 
			//gets all from sales , join sales and stock where the date is equal to the session variable 
			//orders category by ascending order 
			$sql = "SELECT * FROM Sales INNER JOIN Stock ON Sales.stockId=Stock.stockNumber 
			WHERE dateAndTime = '$_POST[dateForm]' ORDER BY stockCategory ASC";
			//table header that prints the date 
			echo "<table>".
						"<tr>
							<th>From: ".$_POST['dateForm']. " ".$time."</th>		
							<th>To: " .$_POST['dateForm']." ".$time2."</th>
						<tr>".
				"</table>";
			//execute function to print database data
			productReport($con,$sql);
			//session variable that stores the date from the date picker 
			$_SESSION["dateAndTime"] = $_POST['dateForm'];
			
		}

	};
	//function to print database data in table 
	function productReport($con,$sql)
	{
		//test query from if, else statements and database connection 
		$result=mysqli_query($con,$sql);
		//sets sum to 0 
		$sum = 0;
		
		//print a table header underneath the date that has category and retail price 
		echo "<table>".
						"<tr>
							<th>Category</th>
							<th>Retail Price(PPU)</th>
						</tr>";	
			//while loop to print data in rows 
			while ($row=mysqli_fetch_array($result))
			{
				echo		"<td>".$row['stockCategory']."</td>
							<td>".$row['totalSalePrice']."</td>
							</tr>";
				$sum+=$row['totalSalePrice'];		//adds all the prices together based on sql data and stores in variable sum
			}
			//a footer for the table that prints the total price 
			echo 	"<tfoot>".
							"<th>Total takings for date</th>".
							"<th>".$sum ."</th>".
					"</tfoot>
				</table>";
	}
//close connection
mysqli_close($con);
?>
</body>
</html>
