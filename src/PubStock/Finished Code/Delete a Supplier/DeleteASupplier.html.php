<?php include 'template.php'; ?><!--include template that contains menu bar and background-->
<!DOCTYPE html>
<html>
<head>
<title> Delete a Supplier</title>
<link rel="stylesheet" type="text/css" href="template.css"><!--link to css stlying-->
</head>
<body>

<script>
<!--javascript function that poulates the input fields with data from the database-->
function populate()
{
	var sel = document.getElementById("listbox");
	var result;
	result = sel.options[sel.selectedIndex].value;
	var personDetails = result.split(',');
//	document.getElementById("display").innerHTML = "The details of the selected person are: " + result;
	document.getElementById("delid").value = personDetails[0];
	document.getElementById("delname").value = personDetails[1];
	document.getElementById("delstreet").value = personDetails[2];
	document.getElementById("deltown").value = personDetails[3];
	document.getElementById("delcounty").value = personDetails[4];
	document.getElementById("delphoneno").value = personDetails[5];
	document.getElementById("delfaxno").value = personDetails[6];
	document.getElementById("delemail").value = personDetails[7];
	document.getElementById("delwebadd").value = personDetails[8];
}

<!--confirm meassage tha appears on screen whe the sumit button is pressed and gets elements by id -->
function confirmCheck()
{
	var response;
	response = confirm('Are you sure you want to delete this person?');
	if (response)
	{
		document.getElementById("delid").disabled = false;
		document.getElementById("delname").disabled = false;
		document.getElementById("delstreet").disabled = false;
		document.getElementById("deltown").disabled = false;
		document.getElementById("delcounty").disabled = false;
		document.getElementById("delphoneno").disabled = false;
		document.getElementById("delfaxno").disabled = false;
		document.getElementById("delemail").disabled = false;
		document.getElementById("delwebadd").disabled = false;
		return true;
	}
	else
	{
		populate();
		return false;
	}
}
</script>	
<!--startt of css styling -->
<div class="content">
<p id = "display"> </p>	
<!--heading -->
<h1>Delete a Supplier</h1>
<!--show on screen a listbox that contains information on a supplier-->
<?php include 'listboxDelete.php'; ?>
<!--start of form for deleteing -->
<form name="deleteForm" action="delete.php" onsubmit="return confirmCheck()" method="post">
<p>
<!--label and input on screen for supplier id -->
<label for "delid">Supplier Id </label>
<input type = "text" name = "delid" id = "delid" disabled>
</p><p>
<!--label and input on screen for supplier name -->
<label for "delname">Supplier Name </label>
<input type = "text" name = "delname" id = "delname" disabled>
</p><p>
<!--label and input on screen for supplier street -->
<label for "delstreet">Street </label>
<input type = "text" name = "delstreet" id = "delstreet" disabled>
</p><p>
<!--label and input on screen for supplier town -->
<label for "deltown">Town </label>
<input type = "text" name = "deltown" id = "deltown" disabled>
</p><p>
<!--label and input on screen for supplier county -->
<label for "delcounty">County </label>
<input type = "text" name = "delcounty" id = "delcounty" disabled>
</p><p>
<!--label and input on screen for supplier phone number -->
<label for "delphoneno">Phone number </label>
<input type = "tel" name = "delphoneno" id = "delphoneno" disabled>
</p><p>
<!--label and input on screen for supplier fax number -->
<label for "delfaxno">Fax number </label>
<input type = "tel" name = "delfaxno" id = "delfaxno" disabled>
</p><p>
<!--label and input on screen for supplier email-->
<label for "delemail">Email </label>
<input type = "email" name = "delemail" id = "delemail" disabled>
</p><p>
<!--label and input on screen for supplier web address -->
<label for "delwebadd">Web address </label>
<input type = "text" name = "delwebadd" id = "delwebadd" disabled>
</p>
<br>
		<!--submit button that collects data and goes to php page -->
		<input type="submit" value = "Submit"/>
		<!--reset buuton-->
		<input type="reset" value = "Clear"/>

<p>
</form><!--end of form-->


</div><!--end of css styling-->

</body>
</html>
