<?php 	include 'template.php'; 
include 'db.inc.php';
session_id('orderSession');
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<title>Manual Ordering</title>
<link rel="stylesheet" type="text/css" href="template.css">
</head>
<body>
<script>
//avascript function that poulates the input fields with data from the database
function populate()
{
	var sel = document.getElementById("listboxSupp");
	var result;
	result = sel.options[sel.selectedIndex].value;
	var Details = result.split(',');
	document.getElementById("hiddenName").value = Details[1];
	document.getElementById("hiddenId").value=Details[0];
}
//confirm meassage tha appears on screen whe the sumit button is pressed and gets elements by id 
function confirmCheck()
	{
		var response;
		response = confirm('Are you sure you want to select this supplier?');
		
		if (response)
			{
    			document.getElementById("hiddenId").disabled = false;
  				return true;
 			}
		else
 			{
				populate();
				return false;
 			}
	}
	
</script>	
<!--start of css styling-->
<div class="content">
<!--heading 1 -->
<h1>Manual Ordering</h1>
<!--start of form for manual ordering-->
<form name="ManualOrdering" action="test.php" onsubmit="confirmCheck()" method="post"> 
<!--heading 4 and include and the lisbox that contains supplier information-->
<h4>Supplier</h4><?php include 'listboxSupplier.php'; ?>
<!--hidden inputs that will populate when a supplier is selected -->
<input type="hidden" name="hiddenName" id = "hiddenName" disabled>
<input type="hidden" name="hiddenId" id="hiddenId" disabled>
<br></br>
<br></br>	
		<!--submit button that goes to manualordering2.php-->
		<input type="submit" name = "submit" />
</form>
	<br></br>
<!--form and submit button that takes you to the Stock Control Menu -->
<form name="ManualOrdering2" action="StockControlMenu.html" method="post"> 
<!--submit button called cancel-->
<input type="submit" name = "Cancel" value = "Cancel" />
</form>
</div>

<!--start of php-->
<?php
	//database connection
	include 'db.inc.php';
	
	//sql statement to show all stock that have suppStockCode and suppId in common	
	$sql = "SELECT * FROM Stock INNER JOIN Supplier ON Stock.suppStockCode=Supplier.suppId";
		
		//test connection and query 
		$result=mysqli_query($con,$sql);
		//show a table on screen of everything that matches the sql statement 
		echo "<table>".
						"<tr>
							<th>Stock Number</th>
							<th>Stock Description</th>
							<th>Quantity Stocked</th>
							<th>Reorder Quantity</th>
							<th>Supplier Stock Code</th>
							<th>Supplier Name</th>
						</tr>";
			//loop to print data from database 
			while ($row=mysqli_fetch_array($result))
			{
				echo		"<td>".$row['stockNumber']."</td>
							<td>".$row['stockDescription']."</td>
							<td>".$row['quantityStocked']."</td>
							<td>".$row['reorderQuantity']."</td>
							<td>".$row['suppStockCode']."</td>
							<td>".$row['suppName']."</td>
							</tr>";

			}
			//end table 
			echo "</table>";
//end connection 
mysqli_close($con);

?>
</body>
</html>
