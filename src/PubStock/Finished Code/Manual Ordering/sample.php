<?php
//start of session called order session
session_id('orderSession');
session_start();

//if the index is set or more tha 0 , gets the data from the hidden input fields in manualOrdering2.html.php 
//and stores them in session variable array 
if(isset($_SESSION['index']))
{		//gets and stores hiddenDesc
		$_SESSION['hiddenDescription'.$_SESSION['index'] ]= $_POST['hiddenDesc'];
		//gets and stores hiddenName
		$_SESSION['hiddenName'.$_SESSION['index'] ]= $_POST['hiddenName'];
		//gets and stores hiddenStreet
		$_SESSION['hiddenStreet'.$_SESSION['index'] ]= $_POST['hiddenStreet'];
		//gets and stores hiddenTown
		$_SESSION['hiddenTown'.$_SESSION['index'] ]= $_POST['hiddenTown'];
		//gets and stores hiddenCounty
		$_SESSION['hiddenCounty'.$_SESSION['index'] ]= $_POST['hiddenCounty'];
		//gets and stores hiddenReorder
		$_SESSION['hiddenReorder'.$_SESSION['index'] ]= $_POST['hiddenReorder'];
		//gets and stores hiddenStockId
		$_SESSION['hiddenStockId'.$_SESSION['index'] ]= $_POST['hiddenStockId'];
		//gets and stores hiddenSuppStockCode
		$_SESSION['hiddenSuppStockCode'.$_SESSION['index'] ]= $_POST['hiddenSuppStockCode'];
		//gets and stores hiddenStockPrice
		$_SESSION['hiddenStockPrice'.$_SESSION['index'] ]= $_POST['hiddenStockPrice'];
		//increment the index
		$_SESSION['index']++;
}
else 	//If the index is 0 store data in session variable array
{
		$_SESSION['index']=0;
		//gets and stores hiddenDesc
		$_SESSION['hiddenDescription'.$_SESSION['index'] ] = $_POST['hiddenDesc'];
		//gets and stores hiddenName
		$_SESSION['hiddenName'.$_SESSION['index'] ]= $_POST['hiddenName'];
		//gets and stores hiddenStreet
		$_SESSION['hiddenStreet'.$_SESSION['index'] ]= $_POST['hiddenStreet'];
		//gets and stores hiddenTown
		$_SESSION['hiddenTown'.$_SESSION['index'] ]= $_POST['hiddenTown'];
		//gets and stores hiddenCounty
		$_SESSION['hiddenCounty'.$_SESSION['index'] ]= $_POST['hiddenCounty'];
		//gets and stores hiddenReorder
		$_SESSION['hiddenReorder'.$_SESSION['index'] ]= $_POST['hiddenReorder'];
		//gets and stores hiddenStockId
		$_SESSION['hiddenStockId'.$_SESSION['index'] ]= $_POST['hiddenStockId'];
		//gets and stores hiddenSuppStockCode
		$_SESSION['hiddenSuppStockCode'.$_SESSION['index'] ]= $_POST['hiddenSuppStockCode'];
		//gets and stores hiddenStockPrice
		$_SESSION['hiddenStockPrice'.$_SESSION['index'] ]= $_POST['hiddenStockPrice'];
		//increment the index
	 	$_SESSION['index']++;

}
//takes you back to manualOrdering2
header("Location:ManualOrdering2.html.php"); /* Redirect to page to enter info again*/
?>
