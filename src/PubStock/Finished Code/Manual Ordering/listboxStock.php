<?php 
include "db.inc.php"; //database connection
//start of session called order session
session_id('orderSession');
session_start();

//sql statement to get all from supplier where supplier stock code and supplier id match and the suppstockcode matched the session variable
//hiiden id which is from manual ordering in a hidden input
$sql = "SELECT * FROM Stock INNER JOIN Supplier ON Stock.suppStockCode=Supplier.suppId WHERE Stock.suppStockCode = '$_SESSION[hiddenID]' ";	

//test query and connection
if (!$result = mysqli_query($con, $sql))
{
	die('Error in querying the database' . mysqli_error($con));
}
//output a listbox 
echo "<br><select name = 'listboxStock[]' id ='listboxStock' onChange = 'populate2()' >";

//get rows of data from database 
while ($row = mysqli_fetch_array($result))
{
	$name = $row['suppName'];
	$suppStreet = $row['suppStreet'];
	$suppTown = $row['suppTown'];
	$suppCounty = $row['suppCounty'];
	$desc = $row['stockDescription'];
	$reorderQuan = $row['reorderQuantity'];
	$id = $row['stockNumber'];
	$suppStock = $row['suppStockCode'];
	$suppId = $row['suppId'];
	$stockPrice = $row['stockPrice'];
	$allText = "$name,$suppStreet,$suppTown,$suppCounty,$desc,$reorderQuan,$id,$suppStock,$suppId,$stockPrice";
	echo "<option value='' disabled selected hidden>Choose a Stock Item...</option>";
	echo "<option value = '$allText'>$desc </option>";
}


echo "</select>";
mysqli_close($con);

?>