<?php
//database connection 
include 'db.inc.php';
// include and show templet which contains menu bar 
include 'template.php';
//start a session called orderSession 
session_id('orderSession');
session_start();
?>
<!--start of css styling -->
<link rel="stylesheet" type="text/css" href="template.css">
<br></br>
<?php
//php variable to get the date for today
$date = date("Y/m/d");

//block of code gets the max from the table and adds one to highest number in OrderTable 
$query = mysqli_query($con,"SELECT MAX(orderNumber) as max FROM OrderTable"); 
$row = mysqli_fetch_array($query);
$highest_id = $row['max'];
$newID = $highest_id + 1;


//Outputs a table based on the session data collected from the last page 
echo "<table>".
				"<tr>
						<th>Order Number</th>
						<th>Supplier Name</th>
						<th>Supplier Address</th>
						<th>Stock Description</th>
						<th>Quantity Ordered</th>
						<th>Order Date</th>
						<th>Stock Number</th>
						<th>Supplier Stock Code</th>
				</tr>";
//for loop to output the session data as list that the user chose in manual ordering 2 
for($i=0; $i<$_SESSION['index']; $i++)
	{
		echo 	"<tr>";
		echo 			"<td>".$newID."</td>".
						"<td>".$_SESSION['hiddenName'.$i]."</td>".
						"<td>".$_SESSION['hiddenStreet'.$i]. $_SESSION['hiddenTown'.$i].$_SESSION['hiddenCounty'.$i]."</td>".
						"<td>".$_SESSION['hiddenDescription'.$i]."</td>".
						"<td>".$_SESSION['hiddenReorder'.$i]."</td>".
						"<td>".$date."</td>".
						"<td>".$_SESSION['hiddenStockId'.$i]."</td>".
						"<td>".$_SESSION['hiddenSuppStockCode'.$i]."</td>".
						"<br>";
		echo 	"</tr>";
   	}
//end table 
echo "</table>";

?>
<!--start of css styling -->
<div class="content">
<!--form and ok button that takes you to a letter pagae that outputs a letter to the supplier-->
<form action="letter.html.php" method="POST">
<br>
<br>
<input type="submit" value="Ok" />

