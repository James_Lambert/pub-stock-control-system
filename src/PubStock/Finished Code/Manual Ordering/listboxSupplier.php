<?php 
include "db.inc.php"; //database connection

//sql statement that gets all fields form supplier 
$sql2 = "SELECT suppId, suppName, suppStreet, suppTown, suppCounty, suppPhoneNumber, suppFaxNumber, suppEmail, suppWebAddress FROM Supplier";

//test query and connection 
if (!$result = mysqli_query($con, $sql2))
{
	die('Error in querying the database' . mysqli_error($con));
}
//output a list box 
echo "<br><select name = 'listboxSupp' id = 'listboxSupp' onchange = 'populate()'>";
//get rows of data from supplier table 
while ($row = mysqli_fetch_array($result))
{
	$id = $row['suppId'];
	$name = $row['suppName'];
	$street = $row['suppStreet'];
	$town = $row['suppTown'];
	$county = $row['suppCounty'];
	$phoneno = $row['suppPhoneNumber'];
	$faxno = $row['suppFaxNumber'];
	$email = $row['suppEmail'];
	$webadd = $row['suppWebAddress'];
	$allText = "$id,$name,$street,$town,$county,$phoneno,$faxno,$email,$webadd $blank";
	echo "<option value='' disabled selected hidden>Choose a Supplier...</option>";
	echo "<option value = '$allText' placeholder='Suuplier' >$name </option>"; 
}

echo "</select>";
mysqli_close($con);

?>