<?php 	
//start a session called orderSession 
session_id('orderSession');
session_start();
// include and show templet which contains menu bar 
include 'template.php'; 
//database connection 
include 'db.inc.php';
?>
<script>
//javascript that populates input fields with selected stock choice from listbox 
function populate2()
{
	var sel = document.getElementById("listboxStock");
	var result;
	result = sel.options[sel.selectedIndex].value;
	var Details = result.split(',');
	document.getElementById("hiddenStockId").value = Details[6];
	document.getElementById("hiddenDesc").value = Details[4];	
	document.getElementById("hiddenSuppStockCode").value = Details[7];
	document.getElementById("hiddenName").value = Details[0];
	document.getElementById("hiddenReorder").value = Details[5];	
	document.getElementById("hiddenStreet").value = Details[1];
	document.getElementById("hiddenTown").value = Details[2];
	document.getElementById("hiddenCounty").value = Details[3];
	document.getElementById("hiddenSuppId").value = Details[8];
	document.getElementById("hiddenStockPrice").value = Details[9];
	
}
//toggles the input field on or off for  amend details 
function toggleLock()
	{
		if (document.getElementById("amendViewbutton").value == "Amend Details")
			{
				document.getElementById("hiddenReorder").disabled = false;	

				document.getElementById("amendViewbutton").value="View Details";
			}
		else
			{
			  	document.getElementById("hiddenReorder").disabled = true;	

				document.getElementById("amendViewbutton").value="Amend Details";
			}
	}
//confirm message to appear when submit button is pressed 
function confirmCheck()
	{
		var response;
		response = confirm('Are you sure you want to save these changes?');
		
		if (response)
			{
    			document.getElementById("hiddenDesc").disabled = false;
				document.getElementById("hiddenReorder").disabled = false;
				document.getElementById("hiddenSuppStockCode").disabled = false;
				document.getElementById("hiddenName").disabled = false;
				document.getElementById("hiddenStockId").disabled = false;
				document.getElementById("hiddenStreet").disabled = false;
				document.getElementById("hiddenTown").disabled = false;
				document.getElementById("hiddenCounty").disabled = false;
				document.getElementById("hiddenSuppId").disabled = false;
				document.getElementById("hiddenStockPrice").disabled = false;
				//document.getElementById("demo").disabled = false;
  				return true;
 			}
		else
 			{
				populate2();
				toggleLock();
				return false;
 			}
	}	
</script>
<br></br>
<?php
//database connection
include 'db.inc.php';
//sql query to slect all from the table where the supplierid matched the suppStockCode and the suppStockCode
//matches the session variable hiddenid from the last screen
$sql = "SELECT * FROM Stock INNER JOIN Supplier ON Stock.suppStockCode=Supplier.suppId
		WHERE suppStockCode = '$_SESSION[hiddenID]'";
			
			//test query and connection 
			$result=mysqli_query($con,$sql);
			
			//outputs a table based on the sql query 
			echo	"<table>".
					"<tr>
							<th>Stock Number</th>
							<th>Stock Description</th>
							<th>Quantity Stocked</th>
							<th>Reorder Quantity</th>
							<th>Supplier Stock Code</th>
							<th>Supplier Name</th>
					</tr>";
			//loop that prints the rows fro the database 			
			while ($row=mysqli_fetch_array($result))
				{
					echo		"<td>".$row['stockNumber']."</td>
								<td>". $row['stockDescription'] . "</td>
								<td>".$row['quantityStocked']."</td>
								<td>".$row['reorderQuantity']."</td>
								<td>".$row['suppStockCode']."</td>
								<td>".$row['suppName']."</td>
								</tr>";

				}
			echo "</table>";//end table 

mysqli_close($con);//end connection 
?>
<!--start of css styling -->
<div class = "content">
	<!--form for manaul ordering that holds hidden inputs goes to a session page on add to list button-->
	<form name="ManualOrdering2" action = "sample.php" onsubmit="confirmCheck()"  method="post">  
		<!--heading 4 and show the listbox that contains BOTH stock and supplier information-->
		<h4>Stock</h4><?php include 'listboxStock.php'; ?>
		
		<!--block of hidden inputs that holds information based on what is selected in the listbox -->
		<!--holds a hiden description for stock item -->
		<input type="hidden" name="hiddenDesc" id = "hiddenDesc" disabled>
		<!--holds a hiden data for supplier name  -->
		<input type="hidden" name="hiddenName" id = "hiddenName" disabled>
		<!--holds a hiden data for supplier stock code  -->
		<input type="hidden" name="hiddenSuppStockCode" id = "hiddenSuppStockCode" disabled>
		<!--holds a hiden data for stock id   -->
		<input type="hidden" name="hiddenStockId" id = "hiddenStockId" disabled>
		<!--holds a hiden data for supplier street -->
		<input type="hidden" name="hiddenStreet" id = "hiddenStreet" disabled>
		<!--holds a hiden data for supplier town -->
		<input type="hidden" name="hiddenTown" id = "hiddenTown" disabled>
		<!--holds a hiden data for supplier county  -->
		<input type="hidden" name="hiddenCounty" id = "hiddenCounty" disabled>
		<!--holds a hiden data for supplier id -->
		<input type="hidden" name="hiddenSuppId" id = "hiddenSuppId" disabled>
		<!--holds a hiden data for stock price   -->
		<input type="hidden" name="hiddenStockPrice" id = "hiddenStockPrice" disabled>
		<br></br>
		<!--heading 4 for reorder quantity -->
		<h4>Reorder Quantity</h4><br></br>
		<!--button that toggles on and off with javascript function -->
		<input type ="button" value="Amend Details" id="amendViewbutton" onclick="toggleLock()">
		<!--hidden input that holds the amended value and the value form the stock table -->
		<input type="text" name="hiddenReorder" id = "hiddenReorder" disabled>
		<br></br>
<br></br>
		<br>
		<br>
		<!--button called at to list that goes to session page -->
		<input type = "submit" name = "submit" value = "Add to List" />
</form>
<!--form and view list button that goes to the next page manualordering3 -->
	<form action = "ManualOrdering3.php" method = "POST">
		<br>
		<br>
		<input type = "submit" value = "View List" />
	</form>
</div>	