<?php
//daatabase connection 
include 'db.inc.php'; 
//start of session called ordersession
session_id('orderSession');
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Supplier Letter</title>
<!--css styling to style the screen as a letter-->
<style type="text/css">
body 
	{
        margin: 10px;
		border: 2px solid black;
    	border-radius: 8px;
		margin-left: 60px;
		margin-right: 60px;
		padding: 20px;
     }
.divHeader 
	{
  		text-align: right;                
		border: 1px solid;        
	}
.divAddress 
	{
		text-align: left;
		float: right;
     }
            
.divSubject 
	{                         
		float : left;
		margin-top: 100px;
     }
.divNumber 
	{              
		               
		margin-top: 120px;
		margin-right: 400px;
		float: right;
     }
.divMessage 
	{                         
		
		margin-top: 160px;
     }
.total
	{
		float: left;
		font-weight: bold;
	}
.Sum
	{
		float: right;
		margin-right: 90px;
		font-weight: bold;
	}
.divSig 
	{              
		margin-left: 920px;               
		padding-top: 50px;         
	}
table, th, td 
	{
    	/*border: 1px solid black;*/
    	border-collapse: collapse;
	}
th, td 
	{
    	padding: 5px;
	}
th 
	{
    	text-align: left;
	}
</style>
</head>
<body>
	<!--div class to bring the name of the pub and adress to the right of the screen -->
	<div class="divAddress">
		The Gin House<br/>
		High Street <br/>
		Carlow <br/> 
		<?php
		//gets today date and outputs 
		$date = date("Y/m/d");
		echo $date;
		?>
		<br/>
	</div>	<!--end of div class-->
	
	<!--start of new div class to keep the supplier name and adress to the left -->
	<div class="divSubject">
	<?php  
	//session variable to output supplier name 
	$i = 0;
	$name = $_SESSION['hiddenName'.$i];
	echo $_SESSION['hiddenName'.$i];
	?>
	<br/>
	<?php
	//session variable to output supplier address 
	$i = 0;
	$address = $_SESSION['hiddenStreet'.$i] . " , " . $_SESSION['hiddenTown'.$i] . " , " . $_SESSION['hiddenCounty'.$i];
	echo $_SESSION['hiddenStreet'.$i] . " , " . $_SESSION['hiddenTown'.$i] . " , " . $_SESSION['hiddenCounty'.$i];
	?>
		
	</div>
	<!--new div class to put order number in center of the scree -->
	<div class="divNumber ">
	<?php
	//block of code gets the max from the table and adds one to highest number in OrderTable 
	$query = mysqli_query($con,"SELECT MAX(orderNumber) as max FROM OrderTable"); 
	$row = mysqli_fetch_array($query);
	$highest_id = $row['max'];
	$newID = $highest_id + 1;
	//output order number 
	echo "Order Number: " .$newID;
	?>
	<br/>
	</div>
</br>
</br>
<!--div class to put the message underneath supplier name and address-->
<div class = "divMessage">
Please supply the following stock.
</div>

<!--div class to put session variables reorder quantity stock number stock description suppliers stocde and price in a table -->
<div class="divContents">
<?php
//php to output session variable in a table 
echo "<table style='width:100%'>.
  	<tr>
    	<th>Quantity</th>
    	<th>Stock Number</th> 
    	<th>Stock Item Description</th>
		<th>Suppliers Stock Code</th>
		<th>Price</th>
  	</tr>";
//for loop to output session variable in rows and columns 
for($i=0; $i<$_SESSION['index']; $i++)
	{
		echo 	"<tr>";
		echo 			"<td>".$_SESSION['hiddenReorder'.$i]."</td>".
						"<td>".$_SESSION['hiddenStockId'.$i]."</td>".
						"<td>".$_SESSION['hiddenDescription'.$i]."</td>".
						"<td>".$_SESSION['hiddenSuppStockCode'.$i]."</td>".
						"<td>".$_SESSION['hiddenStockPrice'.$i]."</td>".
						"<br>";
		echo 	"</tr>";
   	}
echo "</table>";//end table 
	
echo "<br>";

?>
<!--div class to put total cost at the right of letter underneath the table -->
<div class="total">	
Total Cost	
</div>
<!--div class to put the sum of prices from the table  to the right of total cost and underneath the table -->
<div class="Sum">
<?php
//sets total to 0 
$total = 0;
//get all the prices in the session variables and add them together 
for($i=0; $i<$_SESSION['index']; $i++)
{
	$total += $_SESSION['hiddenStockPrice'.$i];
}
//output the total 
echo $total;
?>
</div>
<!--end of div class for table -->	
</div>
<!-- new div class to put name and message on bottom right of screen -->
<div class="divSig">
Yours sincerly, <br/>
<!-- Space for signature. -->
Pub Managers Name
        </div>
<?php
//sets i to 0 
$i = 0;
//get the session variable for quantity ordered at i 0 
$quantityOrdered = $_SESSION['hiddenReorder'. $i];
//get the session variable for supplier stock code at i 0 
$suppStockCode = $_SESSION['hiddenSuppStockCode'. $i];

//sql statement to put all session variables into the Order table 
$sql = 	"INSERT INTO OrderTable(orderNumber,suppName,suppAddress,orderDate,suppStockCode)
		VALUES 		
		('$newID','$name','$address','$date','$suppStockCode')";
//test query and connection 
if (!mysqli_query($con,$sql))
{
	die ("An Error in the SQL Query: " . mysqli_error());
}

//loop that will put multiple pieces of stock that have been selected by the user in the OrderItem table 
// the pieces of stock will have the same order number but will have differennt stock numbers and description etc.
for ($i=0; $i<$_SESSION['index']; $i++)
{
	//get the session variable for stock id 
	$stockNumber =  $_SESSION['hiddenStockId'. $i];
	//get the session variable for quantity ordered 
	$quantityOrdered = $_SESSION['hiddenReorder'. $i];
	//get the session variable for stock decription 
	$description = $_SESSION['hiddenDescription'.$i];

		//sql statement that will add session variables to the order item table 
		$sql2 = "INSERT INTO OrderItem(orderNumber,stockId,stockDesc,quantityOrdered)
				VALUES 		
				('$newID','$stockNumber','$description','$quantityOrdered')";
		//test query and connection
		if (!mysqli_query($con,$sql2))
		{
			die ("An Error in the SQL Query: " . mysqli_error());
		}
}
//close connection
mysqli_close($con);

//destroy and forget the session variables 
session_destroy();
?>
<!--form and submit button that takes you back to manual ordering to choose a new supplier -->
<form action="ManualOrdering.html.php">
		<input type="submit" name = "submit" />
</form>
</body>
</html>