<?php include 'template.php'; ?> <!--get the template and use on screen -->
<!DOCTYPE html>
<html>
<head>
<title> Add a Supplier</title>
<link rel="stylesheet" type="text/css" href="template.css">
</head>
<body>

<script>
	
function confirmCheck()  //Confirm message when you submit the screen
{
	var response;
	response = confirm('Are you sure you want to add this supplier?');
	if (response)
	{
		document.getElementById("suppname").disabled = false;		//gets all the variables
		document.getElementById("street").disabled = false;
		document.getElementById("town").disabled = false;
		document.getElementById("phoneno").disabled = false;
		document.getElementById("faxno").disabled = false;
		document.getElementById("email").disabled = false;
		document.getElementById("webadd").disabled = false;
		return true;
	}	

</script>	
<div class="content"> <!--start the content in css file to style the screen-->

<form action="insert.php" onsubmit="return confirmCheck()" method="Post"> <!--start of form-->
	
	<h1>Add a Supplier</h1>	<!--heading -->

<!--input to enter supplier name and set a pattern to used when you enter-->
<p><label for> Supplier name
<input type="text" name="suppname" id="suppname" placeholder="Supplier name" autocomplete=off  	
	  pattern="^(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d]{2,}$"required/> 									
</label>
</p>
<!--input to enter supplier street and set a pattern to used when you enter-->
<p><label>Street(No comma's etc)
<input type="text" name="street" id="street" placeholder="Street" 
	   "(?=.*[A-Z]){1,}" required/> 
</label>
</p>
<!--input to enter supplier town and set a pattern to used when you enter-->
<p><label>Town
<input type="text" name="town" id="town" placeholder="Town"
	  patter="(?=.*[A-Z]){1,}" required/> 
</label>
</p>
<!--list box for counties -->
<p><label>County
<select id = "countyBox" name = "countyBox">
	<option value='' disabled selected hidden>Choose a County...</option>";
	<option value="Antrim">Antrim</option>
	<option value="Armagh">Armagh</option>
	<option value="Carlow">Carlow</option>
	<option value="Cavan">Cavan</option>
	<option value="Clare">Clare</option>
	<option value="Cork">Cork</option>
	<option value="Derry">Derry</option>
	<option value="Donegal">Donegal</option>
	<option value="Down">Down</option>
	<option value="Dublin">Dublin</option>
	<option value="Fermanagh">Fermanagh</option>
	<option value="Galway">Galway</option>
	<option value="Kerry">Kerry</option>
	<option value="Kildare">Kildare</option>
	<option value="Kilkenny">Kilkenny</option>
	<option value="Laois">Laois</option>
	<option value="Leitrim">Leitrim</option>
	<option value="Limerick">Limerick</option>
	<option value="Longford">Longford</option>
	<option value="Louth">Louth</option>
	<option value="Mayo">Mayo</option>
	<option value="Meath">Meath</option>
	<option value="Monaghan">Monaghan</option>
	<option value="Offaly">Offaly</option>
	<option value="Roscommon">Roscommon</option>
	<option value="Sligo">Sligo</option>
	<option value="Tipperary">Tipperary</option>
	<option value="Tyrone">Tyrone</option>
	<option value="Waterford">Waterford</option>
	<option value="Westmeath">Westmeath</option>
	<option value="Wexford">Wexford</option>
	<option value="Wicklow">Wicklow</option>
</select>	
</label>
</p>
<!--input to enter supplier phone number and set a pattern to used when you enter-->
<p><label>Phone number
<input type="tel" name="phoneno" id="phoneno" placeholder="Phone Number" required/> 
</label>
</p>
<!--input to enter supplier fax number and set a pattern to used when you enter-->
<p><label>Fax Number
<input type="tel" name="faxno" id="faxno" placeholder="Fax Number" required/> 
</label>
</p>
<!--input to enter supplier email and set a pattern to used when you enter-->
<p><label>Email
<input type="email" name="email" id="email" placeholder="email" 
	   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required/> 
</label>
</p>
<!--input to enter supplier web address and set a pattern to used when you enter eg have https-->
<p><label>Web address
<input type="url" name="webadd" id="webadd" placeholder="webadd" 
	   pattern="https?://.+" required/>
</label>
</p>
<br>
		<!--submit button to go to insert.php-->
		<input type="submit" value = "Submit"/>
		<!--reset the data on screen-->
		<input type="reset" value = "Clear"/>

<p>
</form><!--end of form -->


</div><!--end of style sheet content-->

</body>
</html>
