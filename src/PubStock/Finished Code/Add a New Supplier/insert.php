<?php
	include 'db.inc.php';
	include 'template.php';
?>
<div class = "content">
<?php

//block of code gets the max from the table and adds one to highest number in Supplier table
$query = mysqli_query($con,"SELECT MAX(suppId) as max FROM Supplier"); 
$row = mysqli_fetch_array($query);
$highest_id = $row['max'];
$newID = $highest_id + 1;

//Printing out the details the user entered on the previous screen
echo "The details sent down are: <br>";
echo "The Supplier ID is : ". $newID . " <br>";
echo "The Supplier Name is :" . $_POST['suppname'] . "<br>";
echo "Street :" . $_POST['street'] . "<br>";
echo "Town :" . $_POST['town'] . "<br>";
echo "County :" . $_POST['countyBox'] . "<br>";			
echo "Phone number is :" . $_POST['phoneno'] . "<br>";
echo "Fax number is :" . $_POST['faxno'] . "<br>";
echo "Email is :" . $_POST['email'] . "<br>";
echo "Web Address is :" . $_POST['webadd'] . "<br>";
include 'db.inc.php';
//sql statement to insert the values the user entered and put them in the supplier table
$sql = 	"Insert into Supplier 
		(suppId,suppName,suppStreet,suppTown,suppCounty,suppPhoneNumber,suppFaxNumber,suppEmail,suppWebAddress)
		VALUES 		
		('$newID','$_POST[suppname]','$_POST[street]','$_POST[town]','$_POST[countyBox]','$_POST[phoneno]','$_POST[faxno]'
		,'$_POST[email]','$_POST[webadd]')";

//if the query or connection fails error
if (!mysqli_query($con,$sql))
{
	die ("An Error in the SQL Query: " . mysqli_error());
}
//confirm message to show that the details have been added to the supplier table 
echo "<br>A record has been added for " . $newID . " " . $_POST['suppname'] .  " . " ;

//close connection 
mysqli_close($con);

?>
<!-- brings you back to the add supplier screen-->
<form action = "AddASupplier.html.php" method = "POST">
<br>
		
		<input type="submit" value = "Return"/>
		
</form>
</div>	