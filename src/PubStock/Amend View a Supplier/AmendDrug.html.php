<!--Preferred browser is Chrome-->
<!DOCTYPE html>
<html>
	<head>
<!--Title of page-->
	<title>Amend Drug</title>

	<!--Layout items such as font and scaling-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!--link to my css file-->
	<link rel="stylesheet" type="text/css" href="Website.css">
	</head>	

<body>
	
		 <a class="w3-bar-item w3-button w3-hover-black w3-right"></a>
		<div class="w3-top">
		 <a href="#" class="w3-bar-item w3-button"><i class="fa fa-folder-open"></i> File Maintenence </a>
		 <a href="#" class="w3-bar-item w3-button"><i class="fa fa-ambulance "></i> Supplier Accounts</a>
		 <a href="#" class="w3-bar-item w3-button"><i class="fa fa-balance-scale"></i> Stock Control</a>
		 <a href="#" class="w3-bar-item w3-button"><i class="fa fa-bar-chart"></i> Reports</a>
		 <a href="#" class="w3-bar-item w3-button"><i class="fa fa-close"></i> Log Out</a>
		</div>
	
<div align="center" class="w3-content w3-container w3-padding-64">
	<h3 class="w3-center">Amend A Drug</h3><br><br />
	
	
	<div class="formElement">
			<label>Select Drug</label>
		<?php include 'listbox.php'?><br><br />
	</div>
	

	<script>
	
		function populate()
			{
				var sel = document.getElementById("listbox");
				var result;
				result = sel.options[sel.selectedIndex].value;
				var drugDetails = result.split(',');
				document.getElementById("amendid").value = drugDetails[0];
				document.getElementById("amendbname").value = drugDetails[1];
				document.getElementById("amendgname").value = drugDetails[2];
				document.getElementById("amendform").value = drugDetails[3];
				document.getElementById("amendstrength").value = drugDetails[4];
				document.getElementById("amendsID").value = drugDetails[5];
				document.getElementById("amendinstruc").value = drugDetails[6];
				document.getElementById("amendeffects").value = drugDetails[7];
				document.getElementById("amendcprice").value = drugDetails[8];
				document.getElementById("amendrprice").value = drugDetails[9];
				document.getElementById("amendrelevel").value = drugDetails[10];
				document.getElementById("amendrequantity").value = drugDetails[11];
				document.getElementById("amendscode").value = drugDetails[12];
			}
		
		function toggleLock()
			{
				if (document.getElementById("amendViewbutton").value == "Amend Details")
					{
						document.getElementById("amendbname").disabled = false;
						document.getElementById("amendgname").disabled = false;
						document.getElementById("amendform").disabled = false;
						document.getElementById("amendstrength").disabled = false;
						document.getElementById("amendsID").disabled = false;
						document.getElementById("amendinstruc").disabled = false;
						document.getElementById("amendeffects").disabled = false;
						document.getElementById("amendcprice").disabled = false;
						document.getElementById("amendrprice").disabled = false;
						document.getElementById("amendrelevel").disabled = false;
						document.getElementById("amendrequantity").disabled = false;
						document.getElementById("amendscode").disabled = false;
						document.getElementById("amendViewbutton").value = "View Details";
					}
				else
					{
						document.getElementById("amendbname").disabled = true;
						document.getElementById("amendgname").disabled = true;
						document.getElementById("amendform").disabled = true;
						document.getElementById("amendstrength").disabled = true;
						document.getElementById("amendsID").disabled = true;
						document.getElementById("amendinstruc").disabled = true;
						document.getElementById("amendeffects").disabled = true;
						document.getElementById("amendcprice").disabled = true;
						document.getElementById("amendrprice").disabled = true;
						document.getElementById("amendrelevel").disabled = true;
						document.getElementById("amendrequantity").disabled = true;
						document.getElementById("amendscode").disabled = true;
						document.getElementById("amendViewbutton").value = "Amend Details";
					}
			}


			function confirmCheck()
				{
					var response;
					response = confirm('Confirm Details?');
					if (response)
						{
							document.getElementById("amendbname").disabled = false;
							document.getElementById("amendgname").disabled = false;
							document.getElementById("amendform").disabled = false;
							document.getElementById("amendstrength").disabled = false;
							document.getElementById("amendsID").disabled = false;
							document.getElementById("amendinstruc").disabled = false;
							document.getElementById("amendeffects").disabled = false;
							document.getElementById("amendcprice").disabled = false;
							document.getElementById("amendrprice").disabled = false;
							document.getElementById("amendrelevel").disabled = false;
							document.getElementById("amendrequantity").disabled = false;
							document.getElementById("amendscode").disabled = false;
							return true;
						}
					else
						{
							populate();
							toggleLock();
							return false;
						}
				}
	</script>
	<br />
	
<form action="AmendDrug.php" onsubmit="return confirm('Confirm Details?')" method="POST" >
			
	<div class="button">
			<input type="submit" value="Amend Drug" id="amendViewbutton"/><br /><br /><br />
	</div>
	
		<div class="formElement">
			<label for "amendid">Drug ID</label>
			<input type="text" name ="amendid" id="amendid" readonly><br><br />
		</div>
			
		<div class="formElement">
			<label for "amendbname">Brand Name</label>
			<input type="text" name ="amendbname" id="amendbname" disabled><br><br />
		</div>
	
		<div class="formElement">
			<label for "amendgname">Generic Name</label>
			<input type="text" name ="amendgname" id="amendgname" disabled><br><br />
		</div>

		<div class="formElement">
			<label for "amendform">Form</label>
			<select name="amendform" id="amendform" disabled>
				<option value="Tablet">Tablet</option>
				<option value="Ointment">Ointment</option>
				<option value="Vitamin">Vitamin</option>
				<option value="Liquid">Liquid</option>
				<option value="Capsules">Capsules</option>
				<option value="Suppositories">Suppositories</option>
				<option value="Drops">Drops</option>
				<option value="Injection">Injections</option>
				<option value="Implants ">Implants </option>
				<option value="Inhaler">Inhalers</option>
				<option value="Patches">Patches</option>
			</select><br><br />
		</div>


		<div class="formElement">
			<label for "amendstrength">Strength</label>
			<select name="amendstrength" id="amendstrength" disabled>
				<option value="5mg">5mg</option>
				<option value="10ml">10ml</option>
				<option value="20ml">20ml</option>
				<option value="50ml">50ml</option>
				<option value="2.5mg">2.5mg</option>
				<option value="10mg">10mg</option>
				<option value="20mg">20mg</option>
				<option value="50mg">50mg</option>
				<option value="75mg">75mg</option>
				<option value="100mg">100mg</option>
			</select><br><br />
		</div>

		<div class="formElement">
				<label for "amendsID">Supplier ID</label>
				<input type="text" name="amendsID" id="amendsID" disabled><br><br />
		</div>
					
		<div class="formElement">
			<label for "amendinstruc">Usage Instructions</label>
			<input type="text" name="amendinstruc" id="amendinstruc" disabled><br><br />
		</div>


		<div class="formElement">
			<label for "amendeffects">Side Effects</label>
			<input type="text" name="amendeffects" id="amendeffects" disabled><br><br />
		</div>


		<div class="formElement">
			<label for "amendcprice">Cost Price</label>
			<input type="number" step="0.01" min="0" name="amendcprice" id="amendcprice" disabled><br><br />
		</div>


		<div class="formElement">
			<label for "amendrprice">Retail Price</label>
			<input type="number" step="0.01" min="0" name="amendrprice" id="amendrprice" disabled><br><br />
		</div>

		<div class="formElement">
			<label for "amendrelevel">Reorder Level</label>
			<input type="number" name="amendrelevel" id="amendrelevel" disabled><br><br />
		</div>

		<div class="formElement">
			<label for "amendrequantity">Reorder Quantity</label>
			<input type="number" name="amendrequantity" id="amendrequantity" disabled><br><br />
		</div>

		<div class="formElement">
			<label for "amendscode">Supplier Drug Code</label>
			<input type="number" name="amendscode" id="amendscode" disabled><br><br />
		</div>


	
	<div class="button">
			<input type="reset" value="Clear" id="reset"/>
			<input type="submit" value="Submit" id="submit"/><br /><br /><br />
	</div>
	</div>
</form>
	</div>
	
<footer class="w3-center w3-black w3-padding-64 w3-opacity">
  	<a href="AmendDrug.html.php" class="w3-button w3-light-grey"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>

		<p>© Copyright - pH Pharmacy</p>
		<p>The content on this site is meant for informational purposes only, and is not intended for use as official health consultation or recommendations.<br />
		pH Pharmacy takes no responsibility for harm that may result from the use, abuse or misuse of information contained on this site.</p>

</footer>
	
	</body>
</html>
