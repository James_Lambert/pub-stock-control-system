<!DOCTYPE html>
<html>
<head>
<style>
form
	{
		margin: 1em;
		max-width: 500px;
	}
label
	{
		display: inline-block;
		width: 9em;
		margin-right: 1em;
		margin-top: 1em;
		text-align: right;
	}
</style>
</head>
<body>

<h1>Amed/View a Person</h1>
<h4>Please select a person and then click the amend button if you wish to update</h4>

<?php include 'listbox.php'; ?>
<script>
function populate()
{
	var sel = document.getElementById("listbox");
	var result;
	result = sel.options[sel.selectedIndex].value;
	var personDetails = result.split(',');
//	document.getElementById("display").innerHTML = "The details of the selected person are: " + result;
	document.getElementById("amendid").value = personDetails[0];
	document.getElementById("amendname").value = personDetails[1];
	document.getElementById("amendstreet").value = personDetails[2];
	document.getElementById("amendtown").value = personDetails[3];
	document.getElementById("amendcounty").value = personDetails[4];
	document.getElementById("amendphoneno").value = personDetails[5];
	document.getElementById("amendfaxno").value = personDetails[6];
	document.getElementById("amendemail").value = personDetails[7];
	document.getElementById("amendwebadd").value = personDetails[8];
}

function toggleLock()
{
	if (document.getElementById("amendViewbutton").value == "Amend Details")
		{
			document.getElementById("amendname").disabled = false;
			document.getElementById("amendstreet").disabled = false;
			document.getElementById("amendtown").disabled = false;
			document.getElementById("amendcounty").disabled = false;
			document.getElementById("amendphoneno").disabled = false;
			document.getElementById("amendfaxno").disabled = false;
			document.getElementById("amendemail").disabled = false;
			document.getElementById("amendwebadd").disabled = false;
			document.getElementById("amendViewbutton").value = "View Details";
		}
	else
		{
			document.getElementById("amendname").disabled = true;
			document.getElementById("amendstreet").disabled = true;
			document.getElementById("amendtown").disabled = true;
			document.getElementById("amendcounty").disabled = true;
			document.getElementById("amendphoneno").disabled = true;
			document.getElementById("amendfaxno").disabled = true;
			document.getElementById("amendemail").disabled = true;
			document.getElementById("amendwebadd").disabled = true;
			document.getElementById("amendViewbutton").value = "Amend Details";
		}
}

function confirmCheck()
{
	var response;
	response = confirm('Are you sure you want to delete this person?');
	if (response)
	{
//		document.getElementById("amendid").disabled = false;
		document.getElementById("amendname").disabled = false;
		document.getElementById("amendstreet").disabled = false;
		document.getElementById("amendtown").disabled = false;
		document.getElementById("amendcounty").disabled = false;
		document.getElementById("amendphoneno").disabled = false;
		document.getElementById("amendfaxno").disabled = false;
		document.getElementById("amendemail").disabled = false;
		document.getElementById("amendwebadd").disabled = false;
		return true;
	}
	else
	{
		populate();
		toggleLock();
		return false;
	}
}

</script>

<!--<p id = "display"> </p>-->

<form name="myForm" action="AmendView2.php" onsubmit="return confirmCheck()" method="post">	
	
<br>	
<input type = "button" value = "Amend Details" id = "amendViewbutton" onclick = "toggleLock()">
<br>

<label for "amendid">Supplier Id </label>
<input type = "text" name = "amendid" id = "amendid" disabled>
</p><p>
<label for "amendname">Supplier Name </label>
<input type = "text" name = "amendname" id = "amendname" disabled>
</p><p>
<label for "amendstreet">Street </label>
<input type = "text" name = "amendstreet" id = "amendstreet" disabled>
</p><p>
<label for "amendtown">Town </label>
<input type = "text" name = "amendtown" id = "amendtown" disabled>
</p><p>
<label for "amendcounty">County </label>
<input type = "text" name = "amendcounty" id = "amendcounty" disabled>
</p><p>
<label for "amendphoneno">Phone number </label>
<input type = "tel" name = "amendphoneno" id = "amendphoneno" disabled>
</p><p>
<label for "amendfaxno">Fax number </label>
<input type = "tel" name = "amendfaxno" id = "amendfaxno" disabled>
</p><p>
<label for "amendemail">Email </label>
<input type = "email" name = "amendemail" id = "amendemail" disabled>
</p><p>
<label for "amendwebadd">Web address </label>
<input type = "text" name = "amendwebadd" id = "amendwebadd" disabled>


<br><br>
<input type = "submit" value = "Save Changes">
</form>

</body>
</html>